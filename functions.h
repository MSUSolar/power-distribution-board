#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "can_messages.h"

/////////////////////////////////////////////////////
// Relay/contactor variables and Mappings:

volatile bool precharge_state = 0;
// just for precharge rn
// charge and power pins can be controlled with different variable, for rev 2 of PDB

bool precharge_switch_reset = true;  
// this makes sure you flip it off before you can do a precharge on/off cycle again.  
// so it doesnt precharge immediately after you turn it off, if you leave the switch in the on position

const byte PRECHARGE_SWITCH = 11;   
const byte PRECHARGE_LED = 52;
const byte FAN_CONTROL_PIN = 50;
const byte RELAY1_LOW_SIDE = 48;  
const byte RELAY2_HIGH_SIDE = 46;
const byte RELAY3_PRECHARGE = 44;
const byte RELAY4_MPPT = 42;

////////////////////////////////////////////////////////

//can stuff:

//Can frame ID 0x6B0 length 8
/*
 * Byte 0-1 : Pack Current 
 * Byte 2-3 : Pack Inst. Voltage
 * Byte 4   : Pack SOC
 * Byte 5-6 : Pack health
 * Byte 7   : CRC Checksum
 */
//Can frame ID 0x6B1 length 8
/*
 * Byte 0   : High Cell Voltage ID
 * Byte 1   : Low Cell Voltage ID
 * Byte 2-3 : HIGH cell voltage
 * Byte 4-5 : Low Cell voltage 
 * Byte 6   : Blank
 * Byte 7   : CRC Checksum
 */
//Can frame ID 0x6B2 length 8
/*
 * Byte 0-1 : High Thermistor ID
 * Byte 2-3 : Low themistor ID
 * Byte 4-5 : High Temperature
 * Byte 6-7 : Lowest Temperature
 */

 /*typedef struct
{
  uint32_t id;      // Can be either 11 or 29 bit ID
  uint32_t fid;     // Family ID is a somewhat advanced thing. You can ignore it.
  uint8_t rtr;      // Remote Transmission Request - Don't use this.
  uint8_t priority; // Priority for TX frames. Probably not that useful normally
  uint8_t extended; //Set to true for extended frames, false for standard
  uint8_t length;   // Number of data bytes
  BytesUnion data;  // Data bytes
} CAN_FRAME;  */
 
const int frame0_id = 0x6B0;
const int frame1_id = 0x6B1;
const int frame2_id = 0x6B2;
const int filter_id_mask = 0x1FFF;

unsigned int pack_current = 0;
  double pack_current_d = 0;
  double max_pack_current_d = 0;
unsigned int pack_inst_voltage = 0;
  double pack_inst_voltage_d = 0;
byte pack_soc = 0;
  double pack_soc_d = 0;  
unsigned int pack_health = 0;
byte checksum = 0;

 byte high_cell_voltage_id = 0;
 byte low_cell_voltage_id = 0;
unsigned int high_cell_voltage = 0; 
  double high_cell_voltage_d = 0;
unsigned int low_cell_voltage = 1;
  double low_cell_voltage_d = 0;

byte low_voltage_array_index = 0;
double low_voltage_arr[5] = {0,0,0,0,0};

unsigned int high_thermistor_id = 0;
unsigned int low_thermistor_id = 0;
unsigned int high_temperature = 0;
  double high_temperature_d = 0;
unsigned int low_temperature = 0;
  double low_temperature_d = 0;  
  double abs_low_voltage = 5;

///////////////////////////////////////////////////////

// mcp stuff:
const int ADC_ID = 0b1101000;
const byte ADC_CONFIG_BYTE = 0b10011000;
//  set channel 1 example:::   0b00011000    0 00 1 10 00    rdy, channel, ~O/C, sample, gain

const float MCP3425_PGA = 1;  // PGA variable for the MCP3425 (gain factor)

// taken from the datasheet, this is the LSB variable in the conversion equation
const float MCP3425_LSB = 4.096/65540;  // (2 x Vref)/(2^n)    **n bits of resolution

////////////////////////////////////////////////////////////////

/* 
 *  Function that will isolate the pack from the rest of the car
 *  Should this also send a message about the fact that it did it???????
 */
void isolate_pack() {
  // Then open all of the main control relays  
  // Open the negative relay
  digitalWrite(RELAY1_LOW_SIDE, LOW);
  // Open the precharge relay
  digitalWrite(RELAY2_HIGH_SIDE, LOW);
  // Open the positive relay
  digitalWrite(RELAY3_PRECHARGE, LOW);
  // Open the array relay
  digitalWrite(RELAY4_MPPT, LOW);
  Serial.println("PACK ISOLATED!!!!!!!!!!!!!!!");
  Serial.println("///////////////////////////////////////////////////////////");
  delay(10000);
}

/////////////////////////////////////////////////////////////////////////

void check_voltages()
{
  byte below_threshold_count = 0;
  for(byte i = 0;  i < 5; i++)
  {
    if(low_voltage_arr[i] < 2.8)
      below_threshold_count++;
  }
  if (below_threshold_count >= 3)  // if 4 out of 5 reading are below threshold, shut the pack off
  {  isolate_pack();}
}

// */
////////////////////////////////////////////////////////////////////

void check_precharge_switch()   // this is what the current precharge cycle function is.  no e-stop sense yet
{
  if(precharge_state == 0 && digitalRead(PRECHARGE_SWITCH) == LOW && precharge_switch_reset){
    delay(100);
    digitalWrite(RELAY1_LOW_SIDE, HIGH);
    delay(100);
    digitalWrite(RELAY2_HIGH_SIDE, HIGH);
    delay(10000);  // 10 seconds to charge
    digitalWrite(RELAY3_PRECHARGE, HIGH);
    precharge_state = 1;
    precharge_switch_reset = false;
  }
  else if (precharge_state == 1 && digitalRead(PRECHARGE_SWITCH) == LOW && precharge_switch_reset)
  {
    //isolate_pack();
    precharge_state = 0;
    precharge_switch_reset = false;
    delay(300);
  }
  else if (digitalRead(PRECHARGE_SWITCH) == HIGH){
    precharge_switch_reset = true;
    delay(10);  
    }
}

////////////////////////////////////////////////////////////////////////

void frame0_handler(CAN_FRAME *frame)  //6b0
{
  pack_current = frame->data.bytes[0];
  pack_current = pack_current << 8;
  pack_current |= frame->data.bytes[1];
  pack_current_d = (double)pack_current/10;   // going to have to change this value..?
  if(pack_current_d > max_pack_current_d) 
     max_pack_current_d = pack_current_d; 
  
  pack_inst_voltage = frame->data.bytes[2];
  pack_inst_voltage = pack_inst_voltage << 8;
  pack_inst_voltage |= frame->data.bytes[3];
  pack_inst_voltage_d = (double)pack_inst_voltage/10;
  pack_soc |= frame->data.bytes[4];
  pack_soc_d = (double)pack_soc /2;
  pack_health = frame->data.bytes[5];
  pack_health = pack_health << 8;
  pack_health |= frame->data.bytes[6];
  checksum = frame->data.bytes[7];
}


void frame1_handler(CAN_FRAME *frame)  //6b1
{
  if(frame->id != frame1_id)
    Serial.println("shits fucked");
  high_cell_voltage_id = frame->data.bytes[0];
  low_cell_voltage_id = frame->data.bytes[1];
  high_cell_voltage = frame->data.bytes[2];
  high_cell_voltage = high_cell_voltage << 8;
  high_cell_voltage |= frame->data.bytes[3];
  high_cell_voltage_d = (double)high_cell_voltage/10000;  //converts unsigned int into a double
  
  low_cell_voltage = frame->data.bytes[4];
  low_cell_voltage = low_cell_voltage << 8;
  low_cell_voltage |= frame->data.bytes[5];
  low_cell_voltage_d = (double)low_cell_voltage/10000;      //converts unsigned int into a double
  if(low_cell_voltage_d < abs_low_voltage) {         // how im keeping track of the lowest val recorded,
    abs_low_voltage = low_cell_voltage_d; }          // aka did it drop below 2.8 and shut the battery off?

  if(low_voltage_array_index < 5)
  {
    low_voltage_arr[ low_voltage_array_index ] = low_cell_voltage_d;
    low_voltage_array_index++;
  }
  else
  {
    low_voltage_array_index = 0;
    low_voltage_arr[ low_voltage_array_index ] = low_cell_voltage_d;
    low_voltage_array_index++;
  }
}

void frame2_handler(CAN_FRAME *frame)  //6b2
{
  if(frame->id != frame2_id)
    Serial.println("shits fucked");
  high_thermistor_id = frame->data.bytes[0];
  high_thermistor_id = high_thermistor_id << 8;
  high_thermistor_id |= frame->data.bytes[1];
  low_thermistor_id = frame->data.bytes[2];
  low_thermistor_id = low_thermistor_id << 8;
  low_thermistor_id |= frame->data.bytes[3];
  high_temperature = frame->data.bytes[4];
  high_temperature = high_temperature << 8;
  high_temperature |= frame->data.bytes[5];
  high_temperature_d = high_temperature;
  low_temperature = frame->data.bytes[6];
  low_temperature = low_temperature << 8;
  low_temperature |= frame->data.bytes[7];
  low_temperature_d = low_temperature;
}

////////////////////////////////////////////////////////

void print_frame(CAN_FRAME *frame){//this is good for testing to see if you are getting the right values before conversion
   Serial.print("ID: 0x");
   Serial.print(frame->id, HEX);
   Serial.print(" Len: ");
   Serial.print(frame->length);
   Serial.print(" Data: 0x");
   for (int count = 0; count < frame->length; count++) {
       Serial.print(frame->data.bytes[count], HEX);
       Serial.print(" ");
   }
   Serial.print("\r\n");
}

////////////////////////////////////////////////////

void print_vals()
{
  Serial.print("\n\npack_current : ");
  Serial.print(pack_current);
  Serial.print("    ");
  Serial.println(pack_current_d);
  Serial.print("absolute max pack current : ");
  Serial.println(max_pack_current_d);
  
  Serial.print("pack_inst_voltage : ");
  Serial.println(pack_inst_voltage_d);
  Serial.print("pack_soc : ");
  Serial.println(pack_soc_d, 1);
  Serial.print("pack_health : ");
  Serial.println(pack_health);
  Serial.print("high_cell_voltage_id : ");
  Serial.println(high_cell_voltage_id);
  Serial.print("low_cell_voltage_id : ");
  Serial.println(low_cell_voltage_id);
  Serial.print("high_cell_voltage = : ");
  Serial.println(high_cell_voltage_d, 4);
  
  Serial.print("low_cell_voltage : ");
  Serial.println(low_cell_voltage_d, 4);
  Serial.print("absolute lowest cell voltage : ");
  Serial.println(abs_low_voltage, 4);
  
  Serial.print("high_thermistor_id : ");
  Serial.println(high_thermistor_id);
  Serial.print("low_thermistor_id : ");
  Serial.println(low_thermistor_id);
  Serial.print("high_temperature : ");
  Serial.println(high_temperature_d);
  Serial.print("low_temperature : ");
  Serial.println(low_temperature_d); 
}

void print_low_voltage_array()  // testing to see if my corrupted can frame negation idea will work
{
  Serial.print("low voltage array : {");
  for(byte i = 0; i < 4; i++)
  {
    Serial.print(low_voltage_arr[i], 4);
    Serial.print(", ");
  }
  Serial.print(low_voltage_arr[4], 4);
  Serial.println("}");
}

/////////////////////////////////////////////////////

 /*
 * Function to read the value of hte MCP3425 and calculate the current 
 * across the shunt at the moment
 * !!!void for now, but should return 16 bit int num after testing
 */
void read_mcp3425() {
  uint16_t adc_val;
  int message_count = 0;
  
  Wire.requestFrom(ADC_ID, 2);    // request 3 bytes of data from the ADC
  while(Wire.available())   
  {
    if ( message_count == 0 ) {
        adc_val = Wire.read();
        adc_val = adc_val << 8;
    } else if ( message_count == 1) {
        adc_val |= Wire.read();
    } else {
        Wire.read();
    }
    message_count++;
  }
  double voltage = (MCP3425_LSB / MCP3425_PGA) * adc_val;
  //double current = voltage / SHUNT_RESISTANCE;
  Serial.println(voltage, 5);        
  
  //return adc_val;
}
// right now its set up to print the voltage values to the output, but should 
//return the voltage value to complete its sole functions as a helper class (get the voltage)

///////////////////////////////////////////////////////////////////

/* 
 *  Function to handle the hitting of the estop 
 *  Basically just turns on a mosfet to create a temporary connection 
 *    while the mechanical relay is closing
 *    
  CAN_FRAME outgoing;
  outgoing.id = BMS_CAN_BASE + SHITS_FUCKED;
  outgoing.extended = true;
  outgoing.data.high = 0x55AAEE22;
  outgoing.data.low = 0x55AAEE22; 
  outgoing.length = 8;
  Can0.sendFrame(outgoing);

 */



#endif
