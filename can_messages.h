/*
 * File that will hol all of the messages that the BMS wil receive 
 * and eventually the messages that it will send out
 */
#ifndef CAN_MESSAGES_H
#define CAN_MESSAGES_H


// Message that is received to start the precharge
const int PRECHARGE_START_MESSAGE = 0x500;

// Message that is sent to isolate the pack and turn off the car
const int STOP_MESSAGE = 0x501;

const int BMS_CAN_BASE = 0x600;  //all BMS can ID's add to this

const int CELL_VOLTAGE_EXTREMES = 0x0F8; // min/max cell voltages

const int CELL_TEMP_EXTREMES = 0x0F9;  // min/max cell temps

const int EXT_PACK_STATUS = 0x0FD; // pack status base num

const int CELL_OV = 0x1;
const int CELL_UV = 0x2;
const int CELL_OT = 0x4;
const int UNTRUSTED_MEASUREMENT = 0x8;
const int CMU_TIMEOUT = 0x10;
const int VEHICLE_CAN_TIMEOUT = 0x20;
 
const int BIT_MASK = 0x1FFFFFFF;         // Common CAN bit mask

const int NODE_OFFSET = 0x6;




#endif
