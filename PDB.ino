#include <DueTimer.h>
#include <Wire.h>
#include <EnableInterrupt.h> // Due pin interrupt library
#include <due_can.h>         // Due CAN library
#include "can_messages.h"
#include "functions.h"


void setup() {
  // Start the serial monitor 
  Serial.begin(57600);
  delay(300);
  
  // Init all of the relay controls as outputs defaulted to 0 
  pinMode(RELAY1_LOW_SIDE, OUTPUT);
  digitalWrite(RELAY1_LOW_SIDE, 0);
  pinMode(RELAY2_HIGH_SIDE, OUTPUT);
  digitalWrite(RELAY2_HIGH_SIDE, 0);
  pinMode(RELAY3_PRECHARGE, OUTPUT);
  digitalWrite(RELAY3_PRECHARGE, 0);
  pinMode(RELAY4_MPPT, OUTPUT);
  digitalWrite(RELAY4_MPPT, 0);
  pinMode(FAN_CONTROL_PIN, OUTPUT);
  digitalWrite(FAN_CONTROL_PIN, 0);  // Default the fans to be off
  pinMode(PRECHARGE_SWITCH, INPUT_PULLUP);  // pin 11
  pinMode(A2, INPUT); 
  pinMode(DAC0, OUTPUT);
  analogWrite(DAC0, 240); // reference voltage for can tranceiver.  2.5 volts
  pinMode(PRECHARGE_LED, OUTPUT);
  digitalWrite(PRECHARGE_LED, precharge_state);
  
  Can0.begin(250000);  
  //250 kb/s  ****any faster it wont work, rise and fall time of 3.3 volt CAN rx/tx is too long
   
  Can0.setRXFilter(1, frame0_id, filter_id_mask, false);
  Can0.setRXFilter(2, frame1_id, filter_id_mask, false);
  Can0.setRXFilter(3, frame2_id, filter_id_mask, false);
  Can0.setCallback(1, frame0_handler);
  Can0.setCallback(2, frame1_handler);
  Can0.setCallback(3, frame2_handler);
  // can only pass the can frame pointer to callback function,
  //no parameters in .setCallback(...);  
  // */
  
  // Init the I2C bus and Configure the MCP3425
  Wire.begin();
  delay(100);
  Wire.beginTransmission(ADC_ID); 
  Wire.write(ADC_CONFIG_BYTE);
  Wire.endTransmission();

}

/* 
 * "There can be stuff here, but there should not be much if it can be handles 
 * more efficently using interrupts and timers"  -Mr. Gasper 
 */
void loop() {
  check_precharge_switch();
  check_voltages();
  print_low_voltage_array();
  print_vals();
  Serial.print("////////////////////////////////////    ");
  Serial.println(digitalRead(PRECHARGE_SWITCH));
  delay(100);
}

